package com.fidrox.service2.pojo;

public class UserRoles {

	public UserRoles() {
		super();
	}

	public UserRoles(int role_id) {
		super();
		this.role_id = role_id;
	}

	private int role_id;

	private String role_name;

	private String role_code;

	private int status;

	public int getRole_id() {
		return role_id;
	}

	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public String getRole_code() {
		return role_code;
	}

	public void setRole_code(String role_code) {
		this.role_code = role_code;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
