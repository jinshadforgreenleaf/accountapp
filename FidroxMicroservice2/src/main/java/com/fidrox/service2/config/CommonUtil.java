package com.fidrox.service2.config;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;

public class CommonUtil {

	public static SimpleDateFormat sqlDatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat sqlDate = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat sqlTime = new SimpleDateFormat("HH:mm:ss");

	/* Method to get current sql datetime */
	public static String currentSqlDatetime() {
		return sqlDatetime.format(new Date());
	}

	/* Method to get current sql time */
	public static String currentSqlTime() {
		return sqlTime.format(new Date());
	}

	/* Method to get current sql date */
	public static String currentSqlDate() {
		return sqlDate.format(new Date());
	}

	/* Method to get current sql date */
	public static int getAgeFromDOB(String dob) {
		LocalDate birthDate = LocalDate.parse(dob);
		LocalDate currentDate = LocalDate.now();
		if ((birthDate != null) && (currentDate != null)) {
			return Period.between(birthDate, currentDate).getYears();
		} else {
			return 0;
		}
	}

}
