package com.fidrox.service2.config;

public class AppConstants {

	public static class TABLE_NAMES {
		public static final String ACCOUNT = "account";
	}

	public static final int STATUS_ACTIVE = 1;

}
