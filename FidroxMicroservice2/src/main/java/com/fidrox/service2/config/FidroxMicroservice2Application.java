package com.fidrox.service2.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

@ComponentScan({ "com.fidrox" })
@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
public class FidroxMicroservice2Application {

	public static void main(String[] args) {
		SpringApplication.run(FidroxMicroservice2Application.class, args);
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

}
