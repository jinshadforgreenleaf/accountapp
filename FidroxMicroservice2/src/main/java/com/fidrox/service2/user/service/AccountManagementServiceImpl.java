package com.fidrox.service2.user.service;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fidrox.service2.config.CommonUtil;
import com.fidrox.service2.model.Account;
import com.fidrox.service2.pojo.Users;
import com.fidrox.service2.user.dao.AccountManagementDao;

@Service("accountManagementService")
public class AccountManagementServiceImpl implements AccountManagementService {

	@Autowired
	AccountManagementDao dao;

	@Autowired
	private RestTemplate restTemplate;

	public Map createAccount(Account account, Users user) throws Exception {

		int age = CommonUtil.getAgeFromDOB(user.getDate_of_birth());

		if (age >= 18) {
			account.setMinor_indicator('N');
		} else {
			account.setMinor_indicator('Y');
		}

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		URI uri = new URI("http://localhost:8080/api/usermanagement/createuser");

		HttpEntity<Users> httpEntity = new HttpEntity<>(user, headers);

		RestTemplate restTemplate = new RestTemplate();
		Map userDetailsS = restTemplate.postForObject(uri, httpEntity, Map.class);

		if ((boolean) userDetailsS.get("status")) {
			Map userResponse = (Map) userDetailsS.get("user");
			int user_id = (int) userResponse.get("user_id");
			account.setCustomer_id(user_id);
		} else {
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("status", false);
			data.put("message", "Failed when creating");
		}

		return dao.createAccount(account);
	}

	public Account getAccount(int account_id) throws Exception {
		return dao.getAccount(account_id);
	}

}
