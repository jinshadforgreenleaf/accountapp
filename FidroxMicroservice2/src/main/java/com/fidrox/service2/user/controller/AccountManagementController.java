package com.fidrox.service2.user.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fidrox.service2.config.AppConstants;
import com.fidrox.service2.config.CommonUtil;
import com.fidrox.service2.model.Account;
import com.fidrox.service2.pojo.UserRoles;
import com.fidrox.service2.pojo.Users;
import com.fidrox.service2.user.service.AccountManagementService;

@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
@RestController
@RequestMapping("/api/accountmanagement")
public class AccountManagementController {

	@Autowired
	AccountManagementService service;

	@RequestMapping(value = "/createaccount", method = RequestMethod.POST)
	@ResponseBody
	public Map createAccount(@RequestParam(value = "user_name") String user_name,
			@RequestParam(value = "date_of_birth") String date_of_birth, @RequestParam(value = "gender") char gender,
			@RequestParam(value = "phone_number") String phone_number, @RequestParam(value = "user_role") int user_role,
			@RequestParam(value = "account_type") String account_type, @RequestParam(value = "branch") String branch)
			throws Exception {

		Users user = new Users(user_name, date_of_birth, gender, phone_number, new UserRoles(user_role));

		Account account = new Account(account_type, user_name, CommonUtil.currentSqlDatetime(), 0, branch, 'N',
				AppConstants.STATUS_ACTIVE);

		return service.createAccount(account, user);

	}

	@RequestMapping(value = "/getaccount", method = RequestMethod.GET)
	@ResponseBody
	public Account getAccount(@RequestParam(value = "account_id") int account_id) throws Exception {
		return service.getAccount(account_id);
	}

}
