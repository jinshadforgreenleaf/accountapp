package com.fidrox.service2.user.service;

import java.util.Map;

import com.fidrox.service2.model.Account;
import com.fidrox.service2.pojo.Users;

public interface AccountManagementService {

	public Map createAccount(Account account, Users user) throws Exception;

	public Account getAccount(int account_id) throws Exception;

}
