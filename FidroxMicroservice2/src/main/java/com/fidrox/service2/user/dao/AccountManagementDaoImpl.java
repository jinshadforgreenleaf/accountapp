package com.fidrox.service2.user.dao;

import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.fidrox.service2.config.AbstractDao;
import com.fidrox.service2.model.Account;

@Repository("accountManagementDao")
@Transactional
public class AccountManagementDaoImpl extends AbstractDao<Integer, Account> implements AccountManagementDao {

	public Map createAccount(Account account) throws Exception {
		Map<String, Object> data = new HashMap<String, Object>();
		try {

			save(account);

			data.put("account", account);
			data.put("status", true);
			data.put("message", "Created successfully.");

		} catch (Exception e) {
			data.put("status", false);
			data.put("message", "Failed when creating");
		}
		return data;
	}

	public Account getAccount(int account_id) throws Exception {
		return getByKey(account_id);
	}

}
