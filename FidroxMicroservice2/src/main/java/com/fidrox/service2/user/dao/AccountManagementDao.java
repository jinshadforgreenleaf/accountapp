package com.fidrox.service2.user.dao;

import java.util.Map;

import com.fidrox.service2.model.Account;

public interface AccountManagementDao {

	public Map createAccount(Account account) throws Exception;

	public Account getAccount(int account_id) throws Exception;

}
