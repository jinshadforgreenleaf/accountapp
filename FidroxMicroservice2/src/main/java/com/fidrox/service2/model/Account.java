package com.fidrox.service2.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fidrox.service2.config.AppConstants;

@Entity
@Table(name = AppConstants.TABLE_NAMES.ACCOUNT)
public class Account implements Serializable {

	private static final long serialVersionUID = -7988971999010350708L;

	public Account() {
		super();
	}

	public Account(String account_type, String customer_name, String open_date, int customer_id, String branch,
			char minor_indicator, int status) {
		super();
		this.account_type = account_type;
		this.customer_name = customer_name;
		this.open_date = open_date;
		this.customer_id = customer_id;
		this.branch = branch;
		this.minor_indicator = minor_indicator;
		this.status = status;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "account_id")
	private int account_id;

	@Column(name = "account_type")
	private String account_type;

	@Column(name = "customer_name")
	private String customer_name;

	@Column(name = "open_date")
	private String open_date;

	@Column(name = "customer_id")
	private int customer_id;

	@Column(name = "branch")
	private String branch;

	@Column(name = "minor_indicator")
	private char minor_indicator;

	@Column(name = "status")
	private int status;

	public int getAccount_id() {
		return account_id;
	}

	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}

	public String getAccount_type() {
		return account_type;
	}

	public void setAccount_type(String account_type) {
		this.account_type = account_type;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getOpen_date() {
		return open_date;
	}

	public void setOpen_date(String open_date) {
		this.open_date = open_date;
	}

	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public char getMinor_indicator() {
		return minor_indicator;
	}

	public void setMinor_indicator(char minor_indicator) {
		this.minor_indicator = minor_indicator;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
